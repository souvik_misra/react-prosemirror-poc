import React, { Component } from 'react';
//import logo from './logo.svg';
import CustomEditor from './components/CustomEditor';
import './App.css';

const defaultState = {
  content:''
};

const Output = props => <pre id="cont" className="opcon"></pre>

class App extends Component {
  constructor(props){
    super(props);
    this.state = defaultState;
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <CustomEditor 
            onChange={
              param => {
                this.setState(
                  {
                    content: JSON.stringify(param,null,1)
                  },
                  () => {
                    document.getElementById('cont').textContent = this.state.content;
                  });
              }
            }
          />
        </header>
        <Output content={this.state.content} />
      </div>
    );
  }
}

export default App;
