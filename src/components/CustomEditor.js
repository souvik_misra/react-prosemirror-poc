import React from 'react'
//import CircularJSON from 'circular-json';
import { Editor, Floater, MenuBar } from '@aeaton/react-prosemirror'
//import { options, menu } from '@aeaton/react-prosemirror-config-default'
import { options, menu } from '../configs/CustomSchema'
//import {Schema, MarkType} from 'prosemirror-model'
import {toggleMark} from 'prosemirror-commands'
import {keymap/* , baseKeymap */} from 'prosemirror-keymap'
//import {EditorState} from 'prosemirror-state'

//const getJSON = obj => JSON.parse(CircularJSON.stringify(obj,null,2))

const getTierEnabledOptions = opt => {
  let { plugins, schema } = opt;
  console.log('Schema',schema);

  const tier1 =  {
    attrs: { dataType: 'proformaT1' },
    toDOM() { 
      return ['span', { 'class': 'proformaT1' }, 0] 
    },
    parseDOM: [{ tag: 'span.proformaT1' }],
  }; 

  const tier1Map = keymap({ 'Ctrl-q': toggleMark(tier1) });
  
  plugins.push(tier1Map);

  return {
    schema,
    //schema:new Schema(schemaJSON),
    plugins
  }
};
/* 
const customMenuItems = {
  tier1:{
    title:'Tier 1'
  }
}; 
*/

const CustomEditor = ({ value, onChange }) => (
  <Editor    
    options={getTierEnabledOptions(options)}
    onChange={onChange}
    render={({ editor, view }) => (
      <div>
        <MenuBar menu={menu} view={view} />
        <Floater view={view}>
          <MenuBar menu={{ marks: menu.marks }} view={view} />
        </Floater>
        {editor}
      </div>
    )}
  />
)

export default CustomEditor;

